package org.gcube.common.authorization.utils.manager;

import java.util.Collection;
import java.util.SortedSet;
import java.util.TreeSet;

import org.gcube.common.authorization.utils.secret.Secret;
import org.gcube.common.authorization.utils.user.User;
import org.gcube.common.scope.api.ScopeProvider;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class SecretHolder {

	private SortedSet<Secret> secrets;
	
	public SecretHolder() {
		this.secrets = new TreeSet<Secret>();
	}
	
	public SecretHolder(Secret secret) {
		this.secrets = new TreeSet<Secret>();
		addSecret(secret);
	}
	
	public SecretHolder(Collection<Secret> secrets) {
		this.secrets = new TreeSet<Secret>(secrets);
	}
	
	public void addSecret(Secret secret) {
		if(secret!=null) {
			secrets.add(secret);
		}
	}
	
	public void addSecrets(Collection<Secret> secrets) {
		for(Secret secret : secrets){
			addSecret(secret);
		}
	}
	
	public void set() throws Exception {
		boolean first = true;
		for(Secret secret : secrets) {
			/*
			 * Only the most important Secret must set
			 * AuthorizationProvider and ScopeProvider
			 * the others just need to set their token
			 * in the correspondent provider
			 */
			if(first) {
				secret.set();
				first = false;
			}else {
				secret.setToken();
			}
		}
	}
	
	public SortedSet<Secret> getSecrets() {
		return secrets;
	}
	
	public User getUser() {
		for(Secret secret : secrets) {
			try {
				return secret.getUser();
			}catch (Exception e) {
				// trying the next one
			}
		}
		return null;
	}
	
	public String getContext() {
		for(Secret secret : secrets) {
			try {
				return secret.getContext();
			}catch (Exception e) {
				// trying the next one
			}
		}
		return ScopeProvider.instance.get();
	}
	
	public void reset() {
		boolean first = true;
		for(Secret secret : secrets) {
			try {
				if(first) {
					secret.reset();
					first = false;
				}else {
					secret.resetToken();
				}
			}catch (Exception e) {
				// trying the next one
			}
		}
		if(first) {
			ScopeProvider.instance.reset();
		}
	}

}
