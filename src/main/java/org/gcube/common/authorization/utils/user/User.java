package org.gcube.common.authorization.utils.user;

import java.util.Collection;

/**
 * @author Luca Frosini (ISTI-CNR)
 */
public interface User {

	public String getUsername();
	
	public boolean isApplication();
	
	public Collection<String> getRoles();
	
	public void setRoles(Collection<String> roles);
	
	public String getGivenName();
	
	public String getFamilyName();

	/**
	 * @return the full name in the form 'Surname Name' for a person
	 * or the application identifier for an application;
	 */
	public String getFullName();

	/**
	 * @param nameSurname when true the fullname will be formatted as 'Name Surname',
	 * when false the fullname will be formatted as 'Surname Name',
	 * @return the full name according to nameSurname boolean for a person
	 * or the application identifier for an application;
	 */
	public String getFullName(boolean nameSurname);
	
	public String getEmail();
	
	public String getAbout();
	
}
