package org.gcube.common.authorization.utils.secret;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.regex.Pattern;

import org.gcube.common.authorization.client.Constants;
import org.gcube.common.authorization.library.AuthorizationEntry;
import org.gcube.common.authorization.library.ClientType;
import org.gcube.common.authorization.library.exception.AuthorizationException;
import org.gcube.common.authorization.library.provider.ClientInfo;
import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.authorization.library.utils.Caller;
import org.gcube.common.authorization.utils.socialservice.SocialService;
import org.gcube.common.authorization.utils.user.GCubeUser;
import org.gcube.common.authorization.utils.user.User;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class GCubeSecret extends Secret {

	public static final String GCUBE_TOKEN_REGEX = "^([a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}-[a-fA-F0-9]{8,9}){1}$";
	
	protected AuthorizationEntry authorizationEntry;
	
	@Override
	protected void check(String token) throws AuthorizationException {
		super.check(token);
		if(!Pattern.matches(GCubeSecret.GCUBE_TOKEN_REGEX, token)) {
			throw new AuthorizationException("The GUCBE token must comply with the regex " + GCUBE_TOKEN_REGEX);
		}
	}
	
	public GCubeSecret(String token) {
		super(20, token);
	}

	protected AuthorizationEntry getAuthorizationEntry() throws Exception {
		if(authorizationEntry==null) {
			authorizationEntry = Constants.authorizationService().get(token);
		}
		return authorizationEntry;
	}
	
	@Override
	public void setToken() throws Exception {
		SecurityTokenProvider.instance.set(token);
	}
	
	@Override
	public void resetToken() throws Exception {
		SecurityTokenProvider.instance.reset();
	}
	
	@Override
	public ClientInfo getClientInfo() throws Exception {
		return getAuthorizationEntry().getClientInfo();
	}
	
	@Override
	public Caller getCaller() throws Exception {
		ClientInfo clientInfo = getClientInfo();
		String qualifier = authorizationEntry.getQualifier();
		Caller caller = new Caller(clientInfo, qualifier);
		return caller;
	}
	
	@Override
	public String getContext() throws Exception {
		return getAuthorizationEntry().getContext();
	}
	
	@Override
	public Map<String, String> getHTTPAuthorizationHeaders() {
		Map<String, String> authorizationHeaders = new HashMap<>();
		authorizationHeaders.put(org.gcube.common.authorization.client.Constants.TOKEN_HEADER_ENTRY, token);
		return authorizationHeaders;
	}

	@Override
	public boolean isExpired() {
		return false;
	}

	@Override
	public boolean isRefreshable() {
		return false;
	}

	public User getUser() {
		if(user==null) {
			try {
				ClientInfo clientInfo = getClientInfo();
				ClientType clientType = clientInfo.getType();
				
				switch (clientType) {
					case USER:
						user = SocialService.getSocialService().getUser(this);
						break;
	
					default:
						// The client is not an user. Trying to do the best.
						GCubeUser gCubeUser = new GCubeUser();
						gCubeUser.setRoles(new HashSet<>(clientInfo.getRoles()));
						gCubeUser.setUsername(clientInfo.getId());
						gCubeUser.setApplication(true);
						user = gCubeUser;
						break;
				}

			} catch (Exception e) {
				throw new RuntimeException();
			}
		}
		return user;
	}
		
}
