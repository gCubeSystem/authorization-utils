package org.gcube.common.authorization.utils.secret;

import java.util.regex.Pattern;

/**
 * @author Luca Frosini (ISTI - CNR)
 * This call will be no more available in  
 * component Smartgears 4 based
 */
@Deprecated
public class SecretUtility {

	public static final String UUID_REGEX = "^([a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}-[a-fA-F0-9]{8,9}){1}$";
	
	public static Secret getSecretByTokenString(String token) {
		if(Pattern.matches(UUID_REGEX, token)) {
			return new GCubeSecret(token);
		}else {
			return new JWTSecret(token);
		}
	}
	
}
