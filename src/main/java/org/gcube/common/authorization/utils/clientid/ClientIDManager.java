package org.gcube.common.authorization.utils.clientid;

import org.gcube.common.authorization.utils.secret.JWTSecret;
import org.gcube.common.authorization.utils.secret.Secret;
import org.gcube.common.iam.D4ScienceIAMClient;
import org.gcube.common.iam.D4ScienceIAMClientAuthn;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class ClientIDManager implements RenewalProvider {

	protected final String clientId;
	protected final String clientSecret;
	protected D4ScienceIAMClientAuthn d4ScienceIAMClientAuthn;
	
	public ClientIDManager(String clientId, String clientSecret) {
		this.clientId = clientId;
		this.clientSecret = clientSecret;
	}
	
	private JWTSecret getJWTSecret(D4ScienceIAMClientAuthn d4ScienceIAMClientAuthn) {
		String accessToken = d4ScienceIAMClientAuthn.getAccessTokenString();
		JWTSecret jwtSecret = new JWTSecret(accessToken);
		jwtSecret.setRenewalProvider(this);
		return jwtSecret;
	}
	
	public Secret getSecret(String context) throws Exception {
		D4ScienceIAMClient iamClient = D4ScienceIAMClient.newInstance(context);
		d4ScienceIAMClientAuthn = iamClient.authenticate(clientId, clientSecret, context);
		return getJWTSecret(d4ScienceIAMClientAuthn);
	}
	
	@Override
	public Secret renew(String context) throws Exception {
		if(d4ScienceIAMClientAuthn!=null && d4ScienceIAMClientAuthn.canBeRefreshed()) {
			d4ScienceIAMClientAuthn.refresh(clientId, clientSecret);
			return getJWTSecret(d4ScienceIAMClientAuthn);
		}
		return getSecret(context);
	}
	
}
