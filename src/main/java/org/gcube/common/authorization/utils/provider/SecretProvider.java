package org.gcube.common.authorization.utils.provider;

import org.gcube.common.authorization.utils.secret.Secret;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface SecretProvider {

	public Secret getSecret();
	
}
