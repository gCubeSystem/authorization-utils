package org.gcube.common.authorization.utils.clientid;

import org.gcube.common.authorization.utils.secret.Secret;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface RenewalProvider {

	public Secret renew(String context) throws Exception;
}
