package org.gcube.common.authorization.utils.manager;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class SecretManagerProvider {

	public static SecretManagerProvider instance = new SecretManagerProvider();
	
	// Thread local variable containing each thread's ID
    private static final InheritableThreadLocal<SecretManager> thread = new InheritableThreadLocal<SecretManager>() {

		@Override
		protected SecretManager initialValue() {
			return null;
		}

	};
	
	private SecretManagerProvider(){}
    
	public SecretManager get(){
		SecretManager secretManager = thread.get();
		return secretManager;
	}
	
	public void set(SecretManager secretManager){
		thread.set(secretManager);
	}
	
	public void reset(){
		SecretManager secretManager = thread.get();
		if(secretManager!=null) {
			secretManager.reset();
		}
		thread.remove();
	}
	
	public void remove(){
		thread.remove();
	}
}
