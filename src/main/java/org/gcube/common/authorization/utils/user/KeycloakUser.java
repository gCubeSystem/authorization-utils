package org.gcube.common.authorization.utils.user;

import java.util.Collection;

import org.gcube.com.fasterxml.jackson.annotation.JsonIgnore;
import org.gcube.common.keycloak.model.AccessToken;

/**
 * @author Luca Frosini (ISTI-CNR)
 */
public class KeycloakUser extends AccessToken implements User {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = -7083648026885406300L;

	public static final String CLIENT_ID_PROPERTY_OLD_KEY = "clientId";
	public static final String CLIENT_ID_PROPERTY = "client_id";
	
	protected Collection<String> roles;
	protected Boolean application;
	
	@Override
	@JsonIgnore
	public String getUsername() {
		return getPreferredUsername();
	}

	@JsonIgnore
	protected String getClientId() {
		Object clientIdObj = getOtherClaims().get(CLIENT_ID_PROPERTY);
		if(clientIdObj==null) {
			clientIdObj = getOtherClaims().get(CLIENT_ID_PROPERTY_OLD_KEY);
		}
		return clientIdObj==null ? null : clientIdObj.toString();
	}
	
	@Override
	public boolean isApplication() {
		if(application==null) {
			application = getClientId()!=null;
		}
		return application;
	}
	
	@Override
	@JsonIgnore
	public Collection<String> getRoles() {
		return roles;
	}
	
	@Override
	@JsonIgnore
	public void setRoles(Collection<String> roles) {
		this.roles = roles;
	}

	@Override
	public String getAbout() {
		return "";
	}
	
	@Override
	public String getFullName() {
		return getFullName(false);
	}
	
	@Override
	public String getFullName(boolean nameSurname) {
		if(isApplication()) {
			String clientID = getClientId();
			if(clientID==null) {
				clientID = getUsername();
			}
			return clientID; 
		}
		
		StringBuffer stringBuffer = new StringBuffer();
		boolean found = false;
		String surname = getFamilyName();
		String name = getGivenName();
		
		if(nameSurname) {
			if(name!=null && name.trim().length()>0) {
				stringBuffer.append(name.trim());
				found = true;
			}
			if(surname!=null && surname.trim().length()>0) {
				if(found) {
					stringBuffer.append(" ");
				}
				stringBuffer.append(surname.trim());
				found = true;
			}
		}else {
			if(surname!=null && surname.trim().length()>0) {
				stringBuffer.append(surname.trim());
				found = true;
			}
			if(name!=null && name.trim().length()>0) {
				if(found) {
					stringBuffer.append(" ");
				}
				stringBuffer.append(name.trim());
				found = true;
			}
		}
		
		return stringBuffer.toString();
	}

}
