package org.gcube.common.authorization.utils.secret;

import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.gcube.common.authorization.library.provider.AccessTokenProvider;
import org.gcube.common.authorization.library.provider.ClientInfo;
import org.gcube.common.authorization.library.provider.ExternalServiceInfo;
import org.gcube.common.authorization.library.provider.UserInfo;
import org.gcube.common.authorization.library.utils.Caller;
import org.gcube.common.authorization.utils.clientid.RenewalProvider;
import org.gcube.common.authorization.utils.user.KeycloakUser;
import org.gcube.common.authorization.utils.user.User;
import org.gcube.common.iam.OIDCBearerAuth;
import org.gcube.common.scope.impl.ScopeBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class JWTSecret extends Secret {

	private static final Logger logger = LoggerFactory.getLogger(JWTSecret.class);
	
	/**
	 * The interval of time expressed in milliseconds used as guard to refresh the token before that it expires .
	 * TimeUnit has been used to in place of just 
	 * using the number to have a clearer code 
	 */
	public static final long TOLERANCE = TimeUnit.MILLISECONDS.toMillis(200);
	
	protected RenewalProvider renewalProvider; 
	protected Set<String> roles;
	protected ClientInfo clientInfo;
	protected Caller caller;
	protected String context;
	protected OIDCBearerAuth oidcBearerAuth;
	
	public JWTSecret(String token) {
		super(10, token);
		this.oidcBearerAuth = OIDCBearerAuth.fromAccessTokenString(token);
	}
	
	private String getTokenString() {
		try {
			boolean expired = isExpired();
			
			long now = Calendar.getInstance().getTimeInMillis();
			long expireTime = oidcBearerAuth.getAccessToken().getExp()*1000;
			long expireWithTolerance = expireTime-TOLERANCE;
			
			// We consider expired TOLERANCE millisecond in advance to avoid to perform 
			// a requests while the token is expiring and for this reason is rejected
			if(!expired && now>=expireWithTolerance) {
				expired = true;
			}
			
			if(expired && renewalProvider!=null) {
				try {
					JWTSecret renewed = (JWTSecret) renewalProvider.renew(getContext());
					this.token = renewed.token;
				}catch (Exception e) {
					logger.warn("Unable to renew the token with the RenewalProvider. I'll continue using the old token.", e);
				}
			}
		}catch (Exception e) {
			logger.error("Unexpected error in the procedure to evaluate/refresh the current token. I'll continue using the old token.", e);
		}
		return token;
	}
	
	@Override
	public void setToken() throws Exception {
		AccessTokenProvider.instance.set(getTokenString());
	}

	@Override
	public void resetToken() throws Exception {
		AccessTokenProvider.instance.reset();
	}
	
	protected Set<String> getRoles() throws Exception{
		if(roles == null) {
			this.roles = oidcBearerAuth.getRoles();
		}
		return roles;
	}

	@Override
	public ClientInfo getClientInfo() throws Exception {
		if(clientInfo==null) {
			User user = getUser();
			if(user.isApplication()) {
				clientInfo = new ExternalServiceInfo(user.getUsername(), "unknown");
			}else {
				clientInfo = new UserInfo(user.getUsername(), new ArrayList<>(user.getRoles()), user.getEmail(), user.getGivenName(), user.getFamilyName());
			}
		}
		return clientInfo;
	}

	@Override
	public Caller getCaller() throws Exception {
		if(caller==null) {
			caller = new Caller(getClientInfo(), "token");
		}
		return caller;
	}
	
	@Override
	public String getContext() throws Exception {
		if(context==null) {
			String[] audience = oidcBearerAuth.getAccessToken().getAudience();
			for (String aud : audience) {
				if (aud != null && aud.compareTo("") != 0) {
					try {
						String contextToBeValidated = URLDecoder.decode(aud, StandardCharsets.UTF_8.toString());
						ScopeBean scopeBean = new ScopeBean(contextToBeValidated);
						context = scopeBean.toString();
						return context;
					} catch (Exception e) {
						logger.error("Invalid context name for audience {} in access token. Trying next one if any.", aud, e);
					}
				}
			}
			throw new Exception("Invalid context in access token");
		}
		return context;
	}

	@Override
	public String getUsername() throws Exception {
		return oidcBearerAuth.getAccessToken().getPreferredUsername();
	}
	
	@Override
	public Map<String, String> getHTTPAuthorizationHeaders() {
		Map<String, String> authorizationHeaders = new HashMap<>();
		authorizationHeaders.put("Authorization", "Bearer " + getTokenString());
		return authorizationHeaders;
	}

	public void setRenewalProvider(RenewalProvider renewalProvider) {
		this.renewalProvider = renewalProvider;
	}
	
	@Override
	public boolean isExpired() throws Exception {
		return oidcBearerAuth.isExpired();
	}

	@Override
	public boolean isRefreshable() throws Exception {
		return oidcBearerAuth.canBeRefreshed();
	}
	
	@Override
	public User getUser() {
		if(user==null) {
			try {
				ObjectMapper objectMapper = new ObjectMapper();
				String accessTokenString = objectMapper.writeValueAsString(oidcBearerAuth.getAccessToken());
				user = objectMapper.readValue(accessTokenString, KeycloakUser.class);
				user.setRoles(getRoles());
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
		return user;
	}
	
}