package org.gcube.common.authorization.utils.user;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.gcube.com.fasterxml.jackson.annotation.JsonAnyGetter;
import org.gcube.com.fasterxml.jackson.annotation.JsonAnySetter;
import org.gcube.com.fasterxml.jackson.annotation.JsonIgnore;
import org.gcube.com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class GCubeUser implements User {
	
	/**
	 * Used to allow to have any additional properties
	 */
	@JsonIgnore
	protected Map<String, Object> additionalProperties;
	
	@JsonProperty("id")
	protected String username;
	@JsonProperty("roles")
	protected Set<String> roles;
	
	@JsonProperty("given_name")
	protected String givenName;
	@JsonProperty("family_name")
	protected String familyName;
	@JsonProperty("email")
	protected String eMail;
	@JsonProperty("job_title")
	protected String jobTitle;
	
	@JsonProperty("picture")
	protected String picture;
	@JsonProperty("middle_name")
	protected String middleName;
	
	@JsonIgnore
	protected boolean application;
	
	public GCubeUser() {
		this.additionalProperties = new HashMap<>();
		
		// This info are not always present. Setting an empty string to avoid null
		this.givenName = "";
		this.familyName = "";
		this.eMail = "";
		this.jobTitle = "";
		this.picture = "";
		this.middleName = "";
		this.application = false;
	}
	
	
	@Override
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	@Override
	public boolean isApplication() {
		return application;
	}

	public void setApplication(boolean application) {
		this.application = application;
	}
	
	@Override
	public Collection<String> getRoles() {
		return roles;
	}
	
	@Override
	public void setRoles(Collection<String> roles) {
		this.roles = new HashSet<>(roles);
	}
	@Override
	public String getGivenName() {
		return givenName;
	}
	
	@Override
	public String getFamilyName() {
		return familyName;
	}
	
	@Override
	public String getEmail() {
		return eMail;
	}

	@Override
	public String getAbout() {
		return jobTitle;
	}
	
	public String getPicture() {
		return picture;
	}

	public String getMiddleName() {
		return middleName;
	}
	
	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return additionalProperties;
	}

	public void setAdditionalProperties(Map<String, Object> additionalProperties) {
		this.additionalProperties = additionalProperties;
	}

	public Object getAdditionalProperty(String key) {
		return additionalProperties.get(key);
	}

	@JsonAnySetter
	public void setAdditionalProperty(String key, Object value) {
		this.additionalProperties.put(key, value);
	}

	@Override
	public String getFullName() {
		return getFullName(false);
	}

	@Override
	public String getFullName(boolean nameSurname) {
		if(isApplication()) {
			return getUsername();
		}
		
		StringBuffer stringBuffer = new StringBuffer();
		boolean found = false;
		String surname = getFamilyName();
		String name = getGivenName();
		
		if(nameSurname) {
			if(name!=null && name.trim().length()>0) {
				stringBuffer.append(name.trim());
				found = true;
			}
			if(surname!=null && surname.trim().length()>0) {
				if(found) {
					stringBuffer.append(" ");
				}
				stringBuffer.append(surname.trim());
				found = true;
			}
		}else {
			if(surname!=null && surname.trim().length()>0) {
				stringBuffer.append(surname.trim());
				found = true;
			}
			if(name!=null && name.trim().length()>0) {
				if(found) {
					stringBuffer.append(" ");
				}
				stringBuffer.append(name.trim());
				found = true;
			}
		}
		
		return stringBuffer.toString();
	}

}
