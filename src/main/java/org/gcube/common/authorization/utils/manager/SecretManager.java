package org.gcube.common.authorization.utils.manager;

import java.util.Collection;
import java.util.SortedSet;
import java.util.TreeSet;

import org.gcube.common.authorization.utils.provider.SecretProvider;
import org.gcube.common.authorization.utils.secret.Secret;
import org.gcube.common.authorization.utils.secret.SecretUtility;
import org.gcube.common.authorization.utils.user.User;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class SecretManager {

	private SecretHolder initialSecretHolder;
	private SecretHolder currentSecretHolder;

	public SecretManager() {
		initialSecretHolder = new SecretHolder();
		currentSecretHolder = initialSecretHolder;
	}

	public synchronized void addSecretViaProvider(SecretProvider secretProvider) {
		if (currentSecretHolder != initialSecretHolder) {
			throw new RuntimeException("You can't add a Secret in a session. You must terminate the session first.");
		}
		Secret secret = secretProvider.getSecret();
		currentSecretHolder.addSecret(secret);
	}

	public synchronized void addSecret(Secret secret) {
		if (currentSecretHolder != initialSecretHolder) {
			throw new RuntimeException("You can't add a Secret in a session. You must terminate the session first.");
		}
		currentSecretHolder.addSecret(secret);
	}

	public synchronized void startSession(Secret secret) throws Exception {
		if (currentSecretHolder != initialSecretHolder) {
			throw new RuntimeException("You are already in a session. You must terminate the session first.");
		}
		initialSecretHolder.reset();
		currentSecretHolder = new SecretHolder(secret);
		currentSecretHolder.set();
	}

	public synchronized void startSession(Collection<Secret> secrets) throws Exception {
		if (currentSecretHolder != initialSecretHolder) {
			throw new RuntimeException("You are already in a session. You must terminate the session first.");
		}
		initialSecretHolder.reset();
		currentSecretHolder = new SecretHolder(secrets);
		currentSecretHolder.set();
	}
	
	public synchronized void startSession(SecretHolder secretHolder) throws Exception {
		if (currentSecretHolder != initialSecretHolder) {
			throw new RuntimeException("You are already in a session. You must terminate the session first.");
		}
		initialSecretHolder.reset();
		currentSecretHolder = secretHolder;
		currentSecretHolder.set();
	}

	public synchronized void endSession() {
		if (currentSecretHolder != initialSecretHolder) {
			currentSecretHolder.reset();
			try {
				initialSecretHolder.set();
			}catch (Exception e) {
				throw new RuntimeException(e);
			}
			currentSecretHolder = initialSecretHolder;
		}
	}
	
	public synchronized void set() throws Exception {
		if (currentSecretHolder != initialSecretHolder) {
			throw new Exception("You are in a session. You must terminate the session first.");
		}
		currentSecretHolder.set();
	}

	public synchronized void reset() {
		currentSecretHolder.reset();
		if (initialSecretHolder != currentSecretHolder) {
			initialSecretHolder.reset();
		}
	}

	public synchronized String getContext() {
		return currentSecretHolder.getContext();
	}

	public synchronized User getUser() {
		return currentSecretHolder.getUser();
	}

	/**
	 * @return a copy of the current secret holder
	 * to avoid modification to the original
	 */
	public synchronized SecretHolder getCurrentSecretHolder() {
		SecretHolder secretHolder = new SecretHolder();
		SortedSet<Secret> secrets = new TreeSet<>();
		SortedSet<Secret> originalSecrets = currentSecretHolder.getSecrets();
		for(Secret s : originalSecrets) {
			Secret secret = SecretUtility.getSecretByTokenString(s.getToken());
			secrets.add(secret);
		}
		secretHolder.addSecrets(secrets);
		return secretHolder;
	}
}
