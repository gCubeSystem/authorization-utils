package org.gcube.common.authorization.utils.manager;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.gcube.common.authorization.library.utils.Caller;
import org.gcube.common.authorization.utils.ContextTest;
import org.gcube.common.authorization.utils.clientid.ClientIDManager;
import org.gcube.common.authorization.utils.secret.GCubeSecret;
import org.gcube.common.authorization.utils.secret.JWTSecret;
import org.gcube.common.authorization.utils.secret.Secret;
import org.gcube.common.authorization.utils.user.User;
import org.gcube.common.iam.D4ScienceIAMClient;
import org.gcube.common.iam.D4ScienceIAMClientAuthn;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class SecretManagerTest extends ContextTest {

	private static final Logger logger = LoggerFactory.getLogger(SecretManagerTest.class);
	
	@Test
	public void test() throws Exception {
		SecretManager secretManager = SecretManagerProvider.instance.get();
		SecretHolder secretHolder = secretManager.getCurrentSecretHolder();
		Set<Secret> secrets = secretHolder.getSecrets();
		for(Secret s : secrets) {
			logger.debug("{}: token={}", s.getClass().getSimpleName(), s.getToken());
			User user = s.getUser();
			String username = user.getUsername();
			String surnameName =  user.getFullName();
			String nameSurname =  user.getFullName(true);
			logger.debug("{} - {} - {}", username, surnameName, nameSurname);
		}
	}
	
	// @Test
	public void testJWToken() throws Exception {
		Secret secret = new JWTSecret("");
		User user = secret.getUser();
		String username = user.getUsername();
		String surnameName =  user.getFullName();
		String nameSurname =  user.getFullName(true);
		logger.debug("{} - {} - {}", username, surnameName, nameSurname);
	}
	
	@Test
	public void testClientIDManager() throws Exception {
		Properties properties = new Properties();
		InputStream input = ContextTest.class.getClassLoader().getResourceAsStream(CONFIG_INI_FILENAME);
		try {
			// load the properties file
			properties.load(input);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		String context = DEFAULT_TEST_SCOPE;
		
		int index = context.indexOf('/', 1);
		String root = context.substring(0, index == -1 ? context.length() : index);
		String clientId = properties.getProperty(CLIENT_ID_PROPERTY_KEY);
		String clientSecret = properties.getProperty(root);
		
		if(clientId==null || clientId.compareTo("")==0 || clientSecret==null || clientSecret.compareTo("")==0) {
			return;
		}
		
		ClientIDManager clientIDManager = new ClientIDManager(clientId, clientSecret);
		Secret secret = clientIDManager.getSecret(context);
		Map<String, String> map = secret.getHTTPAuthorizationHeaders();
		logger.debug("Generated HTTP Header {}", map);
		
		Map<String, String> newMap = clientIDManager.renew(context).getHTTPAuthorizationHeaders();
		logger.debug("Refreshed HTTP Header {}", newMap);
		
		Assert.assertTrue(map.size()==newMap.size());
		for(String key : map.keySet()) {
			Assert.assertTrue(map.get(key).compareTo(newMap.get(key))!=0);
		}
	}

	@Test
	public void refreshClientIDTokenTest() throws Exception {
		Properties properties = new Properties();
		InputStream input = ContextTest.class.getClassLoader().getResourceAsStream(CONFIG_INI_FILENAME);
		try {
			// load the properties file
			properties.load(input);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		String context = DEFAULT_TEST_SCOPE;
		
		int index = context.indexOf('/', 1);
		String root = context.substring(0, index == -1 ? context.length() : index);
		String clientId = properties.getProperty(CLIENT_ID_PROPERTY_KEY);
		String clientSecret = properties.getProperty(root);
		
		if(clientId==null || clientId.compareTo("")==0 || clientSecret==null || clientSecret.compareTo("")==0) {
			return;
		}
		
		D4ScienceIAMClient iamClient = D4ScienceIAMClient.newInstance(context);
		D4ScienceIAMClientAuthn d4ScienceIAMClientAuthn = iamClient.authenticate(clientId, clientSecret, context);
		
		String accessToken = d4ScienceIAMClientAuthn.getAccessTokenString();
		logger.info("Generated Access Token is {}", accessToken);
		
		if(d4ScienceIAMClientAuthn!=null && d4ScienceIAMClientAuthn.canBeRefreshed()) {
		    d4ScienceIAMClientAuthn.refresh(clientId, clientSecret);
		    
		    String refreshedAccessToken = d4ScienceIAMClientAuthn.getAccessTokenString();
		    logger.info("Refreshed Access Token is {}", refreshedAccessToken);
			Assert.assertTrue(accessToken.compareTo(refreshedAccessToken)!=0);
		}
	}

	@Ignore
	@Test
	public void testGcubeToken() throws Exception {
		GCubeSecret gCubeSecret = new GCubeSecret("");
		Caller caller = gCubeSecret.getCaller();
		logger.debug("caller {}", caller);
		User user = gCubeSecret.getUser();
		logger.debug("user {}", user);	
	}
	
}
