This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for Authorization Utils

## [v2.3.0-SNAPSHOT]

- Added support for 'client_id' and backward compatibility with 'clientId' claim [#25802, #27389]
- Deprecated class which will be no longer available in Smartgears 4 based components
- Switched to d4science-iam-client in place of keycloak-client [#28357]
- The function getRoles() returns the union of global roles and context roles [#28836]
- Upgraded maven-parent to version 1.2.0


## [v2.2.0]

- Switched to the new version of keycloak-client [#25295]


## [v2.1.0]

- Added remove() method in SecretManagerProvider
- Enhanced gcube-bom version


## [v2.0.0] 

- Refactored code to be integrated in Smartgears [#22871] 
- Fixed getRoles for JWTSecret [#22754]


## [v1.0.0] 

- First Release

